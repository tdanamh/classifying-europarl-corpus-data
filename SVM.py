import numpy as np
import re
import os
from collections import defaultdict
import time
from sklearn import svm
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import KFold

# SUBMISIA 10

FIRST_N_WORDS = 10000


def construire_matrice_confuzie(what_is_true, etichete_prezise):
    # valorile claselor de la 0-11 trebuie reprezentate ca int8
    etichete_prezise = np.array(etichete_prezise).astype('int8')
    what_is_true = np.array(what_is_true)
    matrice_confuzie = np.zeros((11, 11))
    # print("What is true", len(what_is_true))
    # print("Predicted labels", len(etichete_prezise))

    for adevarat, prezis in zip(what_is_true, etichete_prezise):
        matrice_confuzie[adevarat][prezis] += 1
    return matrice_confuzie


# Acuratetea = cate etichete prezise sunt egale cu ce e adevarat?


def acuratete(etichete_prezise,  what_is_true):
    etichete_prezise = np.array(etichete_prezise)
    what_is_true = np.array(what_is_true)
    # vector cu True/False care pune doar True
    boolean_index = (etichete_prezise == what_is_true)

    return len(what_is_true[boolean_index]) * 100 / len(what_is_true)


def files_in_folder(path):
    files = []
    for file in os.listdir(path):
        if os.path.isfile(os.path.join(path, file)):
            files.append(os.path.join(path, file))
    return sorted(files)


def files_without_extension(path):
    file_name = os.path.basename(path)
    file_name_without_extension = file_name.replace('.txt', '')
    return file_name_without_extension


def read_texts_from_directory(path):
    text_data = []
    text_id = []
    for file in files_in_folder(path):
        text_id.append(files_without_extension(file))
        with open(file, 'r', encoding='utf-8') as f:
            text = f.read()

        # text_stripped = re.sub("[-.,;:!?\"\'/()_*=`]", "", text)
        text_stripped = re.sub("[.,;:!?\"/()_*=`]", "", text)

        # lowercase
        # text_stripped = text_stripped.lower()
        # text = text.lower()

        text_data.append(text_stripped.split())
        # text_data.append(text.split())

    return text_id, text_data


# incarcare date
dir_path = 'date_proiect/'
train_path = os.path.join(dir_path, 'train')
test_path = os.path.join(dir_path, 'test')
labels_train = np.loadtxt(dir_path + 'labels_train.txt', 'int8')

# citire cuvinte din texte
train_id, train_data = read_texts_from_directory(train_path)
test_id, test_data = read_texts_from_directory(test_path)

# print(type(train_data))
# print(type(labels_train))


# dictionar cu toate cuvintele unice din exemplele de antrenare cu frecventa cuvintelor
word_dictionary = defaultdict(int)

for document in train_data:
    for word in document:
        word_dictionary[word] += 1

# Dictionarul trebuie transformat in lista de tupluri (cuvant,frecventa_cuvant)
words_frequencies = list(word_dictionary.items())

# sortam descrescator dupa frecventa
words_frequencies = sorted(words_frequencies, key = lambda kv: kv[1], reverse = True)
# print(words_frequencies)

# extragem primele FIRST_N_WORDS cele mai frecvente cuvinte din toate textele
k = 12
# elimin primele k cuvinte cele mai frecvente, considerandu-le prea uzuale
small_word_dictionary = words_frequencies[k: FIRST_N_WORDS]

# luam primele 1000 de cuvinte cele mai frecvente
list_of_selected_words = []
for cuvant, frecventa in small_word_dictionary:
    list_of_selected_words.append(cuvant)


# Repr BOW pt un text impartit in cuvinte in functie de list_of_selected_words


def get_bow_representation_for_text(text, list_of_selected_words):
    bow_representation = defaultdict(int)
    # frecventa initiala 0 pt toate cuvintele din list_of_selected_words
    for word in list_of_selected_words:
        bow_representation[word] = 0
    # pentru fiecare cuvant din text, daca este in list_of_selected_words crestem frecventa
    for word in text:
        if word in list_of_selected_words:
            bow_representation[word] += 1

    return bow_representation.values()


# print("Repr BOW\n", get_bow_representation_for_text(train_data[0], list_of_selected_words))


def get_bow_representation_matrix(matrix, list_of_selected_words, normalizare):

    bow_representation_matrix = np.zeros((len(matrix), len(list_of_selected_words)),'float32')

    for index_text, document in enumerate(matrix):
        # deoarece bow for text = defaultdict -> il transformam in lista
        bow_representation = list(get_bow_representation_for_text(document, list_of_selected_words))
        # transformam bow in np array ca sa putem calcula norma
        bow_representation = np.array(bow_representation)

        if normalizare == 'l2':
            bow_representation = bow_representation / np.sqrt(np.sum(bow_representation ** 2))
        elif normalizare == 'l1':
            bow_representation = bow_representation / np.sum(bow_representation)

        bow_representation_matrix[index_text] = bow_representation

    return bow_representation_matrix


# Pt proiect

# luam reprezentarea bow pt datele de antrenare
train_data_bow_representation = get_bow_representation_matrix(train_data, list_of_selected_words, 'l2')

# luam reprezentarea bow pt datele de test
test_data_bow_representation = get_bow_representation_matrix(test_data, list_of_selected_words, 'l2')

# Clasificator

# definire
# clasificator = svm.SVC(100, 'poly')
clasificator_linearSVC = svm.LinearSVC(C=100)

# antrenare
date1 = time.time()
clasificator_linearSVC.fit(train_data_bow_representation,labels_train)
date2 = time.time()

# timp antrenare
timp = date2 - date1
print("Timp antrenare clasificator", timp)

# predictii
predicted_labels = clasificator_linearSVC.predict(test_data_bow_representation)

testId = np.arange(2984, 4480+1)

# Salvare CSV
np.savetxt("submisie10.csv", np.stack((testId, predicted_labels)).T,
           fmt="%d", delimiter=',', header="Id,Prediction", comments='')


# # Pentru teste locale
#
# # impartim datele de antrenare in: 2500 date de antrenare, 483 date de test
# # impartim etichetele in aceeasi maniera
# data_test = train_data[2500:2983]
# test_labels = labels_train[2500:2983]
#
# # luam reprezentarea bow pt datele de antrenare
# data_train_validation_bow = get_bow_representation_matrix(train_data[:2500], list_of_selected_words, 'l2')
#
# # luam reprezentarea bow pt datele de testare
# data_test_bow = get_bow_representation_matrix(data_test, list_of_selected_words, 'l2')
#
# # Clasificator
#
# # definire
# clasificator_linearSVC = svm.LinearSVC(C=100)
#
# # antrenare
# date1 = time.time()
# clasificator_linearSVC.fit(data_train_validation_bow, labels_train[:2500])
# date2 = time.time()
#
# # timp antrenare
# timp = date2 - date1
# print("Timp antrenare", timp)
#
# # predictii
# predicted_labels_test = clasificator_linearSVC.predict(data_test_bow)
# # print(type(predicted_labels_test))
#
# # acuratete
# print("Acuratete", acuratete(predicted_labels_test, test_labels))


# Pentru K-fold Cross-Validation

# definire clasificator
clasificator_linearSVC = svm.LinearSVC(C=100)

k = 10
kfold = KFold(k)
data = train_data
accuracies = []

for train_index, test_index in kfold.split(data):
    test_start = test_index[0]
    test_end = test_index[-1]
    train_start = train_index[0]
    train_end = train_index[-1]

    data_test = data[test_start: test_end + 1]
    test_labels = labels_train[test_start:test_end + 1]

    if test_end < train_start:
        data_train = data[train_start: train_end + 1]
        train_labels = labels_train[train_start: train_end + 1]
    elif test_start > train_end:
        data_train = data[train_start: train_end + 1]
        train_labels = labels_train[train_start: train_end + 1]
    else:
        data_train = data[train_start:test_start] + data[test_end:train_end + 1]
        train_labels = np.concatenate((labels_train[train_start:test_start], labels_train[test_end:train_end + 1]), axis = None)

    # Reprezentari BOW pt datele de antrenare, testare
    data_train_bow = get_bow_representation_matrix(data_train, list_of_selected_words, 'l2')
    data_test_bow = get_bow_representation_matrix(data_test, list_of_selected_words, 'l2')

    # timp antrenare
    date1 = time.time()
    clasificator_linearSVC.fit(data_train_bow, train_labels)
    date2 = time.time()
    timp = date2 - date1
    print("Timp antrenare", timp)

    # prezicere etichete
    predicted_labels_test = clasificator_linearSVC.predict(data_test_bow)

    # acuratete
    accuracy = acuratete(predicted_labels_test, test_labels)
    print("Acuratete", accuracy)
    accuracies.append(accuracy)

    # matrice de confuzie
    matrice_confuzie = confusion_matrix(test_labels, predicted_labels_test)
    print("Matrice confuzie \n", matrice_confuzie)

print("Vector acuratete", accuracies)
print("Acuratete medie ", np.mean(accuracies))
