import numpy as np
import re
import os
from collections import defaultdict
import time
from sklearn.naive_bayes import GaussianNB

FIRST_N_WORDS = 1000

# SUBMISIA 6

# Acuratetea = cate etichete prezise sunt egale cu ce e adevarat?


def acuratete(etichete_prezise,  what_is_true):
    etichete_prezise = np.array(etichete_prezise)
    what_is_true = np.array(what_is_true)
    # vector cu True/False care pune doar True
    boolean_index = (etichete_prezise == what_is_true)

    return len(what_is_true[boolean_index]) * 100 / len(what_is_true)


def files_in_folder(path):
    files = []
    for file in os.listdir(path):
        if os.path.isfile(os.path.join(path, file)):
            files.append(os.path.join(path, file))
    return sorted(files)


def files_without_extension(path):
    file_name = os.path.basename(path)
    file_name_without_extension = file_name.replace('.txt', '')
    return file_name_without_extension


def read_texts_from_directory(path):
    text_data = []
    text_id = []
    for file in files_in_folder(path):
        text_id.append(files_without_extension(file))
        with open(file, 'r', encoding='utf-8') as f:
            text = f.read()

        # text_stripped = re.sub("[-.,;:!?\"\'/()_*=`]", "", text)
        # text_stripped = re.sub("[.,;:!?\"/()_*=`]", "", text)

        # lowercase
        # text_stripped = text_stripped.lower()
        # text = text.lower()

        # text_data.append(text_stripped.split())
        text_data.append(text.split())

    return text_id, text_data


# incarcare date
dir_path = 'date_proiect/'
train_path = os.path.join(dir_path, 'train')
test_path = os.path.join(dir_path, 'test')
labels_train = np.loadtxt(dir_path + 'labels_train.txt', 'int8')

# citire cuvinte din texte
train_id, train_data = read_texts_from_directory(train_path)
test_id, test_data = read_texts_from_directory(test_path)

# dictionar cu toate cuvintele unice din exemplele de antrenare cu frecventa cuvintelor
word_dictionary = defaultdict(int)

for document in train_data:
    for word in document:
        word_dictionary[word] += 1

# Dictionarul trebuie transformat in lista de tupluri (cuvant,frecventa_cuvant)
words_frequencies = list(word_dictionary.items())

# sortam descrescator dupa frecventa
words_frequencies = sorted(words_frequencies, key = lambda kv: kv[1], reverse = True)
# print(words_frequencies)

# extragem primele 1000 cele mai frecvente cuvinte din toate textele
small_word_dictionary = words_frequencies[0: FIRST_N_WORDS]

# luam primele 1000 de cuvinte cele mai frecvente
list_of_selected_words = []
for cuvant, frecventa in small_word_dictionary:
    list_of_selected_words.append(cuvant)


# Repr BOW pt un text impartit in cuvinte in functie de list_of_selected_words


def get_bow_representation_for_text(text, list_of_selected_words):
    bow_representation = defaultdict(int)
    # frecventa initiala 0 pt toate cuvintele din list_of_selected_words
    for word in list_of_selected_words:
        bow_representation[word] = 0
    # pentru fiecare cuvant din text, daca este in list_of_selected_words crestem frecventa
    for word in text:
        if word in list_of_selected_words:
            bow_representation[word] += 1

    return bow_representation.values()


# print("Repr BOW\n", get_bow_representation_for_text(train_data[0], list_of_selected_words))


def get_bow_representation_matrix(matrix, list_of_selected_words, normalizare):

    bow_representation_matrix = np.zeros( (len(matrix), len(list_of_selected_words) ))

    for index_text, document in enumerate(matrix):
        # deoarece bow for text = defaultdict -> il transformam in lista
        bow_representation = list(get_bow_representation_for_text(document, list_of_selected_words))
        # transformam bow in np array ca sa putem calcula norma
        bow_representation = np.array(bow_representation)

        if normalizare == 'l2':
            bow_representation = bow_representation / np.sqrt(np.sum(bow_representation ** 2))
        elif normalizare == 'l1':
            bow_representation = bow_representation / np.sum(bow_representation)

        bow_representation_matrix[index_text] = bow_representation

    return bow_representation_matrix


# Pt proiect

# luam reprezentarea bow pt datele de antrenare
train_data_bow_representation = get_bow_representation_matrix(train_data, list_of_selected_words, 'l')

# luam reprezentarea bow pt datele de test
test_data_bow_representation = get_bow_representation_matrix(test_data, list_of_selected_words, 'l')

# Clasificator

# definire
# priors=None, var_smoothing= 1e-09 parametrii by default
# e cea mai simpla distributie pentru clasificatorul Naive Bayes

# priors = array cu prob claselor
# var_smoothing = cea mai mare varianta a tuturor feature-urilor care se adauga la stabilitatea calculului

# Gaussian-> valorile asociate fiecarei clase sunt distribuite conform distributiei gaussiane(normale)
#
clasificator_naive_bayes = GaussianNB()

# antrenare
date1 = time.time()
clasificator_naive_bayes.fit(train_data_bow_representation,labels_train)
date2 = time.time()

# timp antrenare
timp = date2 - date1
print("Timp antrenare", timp)

# predictii
predicted_labels = clasificator_naive_bayes.predict(test_data_bow_representation)

testId = np.arange(2984, 4480+1)

# Salvare CSV
np.savetxt("submisie6.csv", np.stack((testId, predicted_labels)).T,
           fmt="%d", delimiter=',', header="Id,Prediction", comments='')



# # Pt teste locale
#
# # impartim datele de antrenare in: 2500 date de antrenare, 483 date de test
# # impartim etichetele in aceeasi maniera
# data_test = train_data[2500:2983]
# test_labels = labels_train[2500:2983]
#
# # luam reprezentarea bow pt datele de antrenare
# data_train_validation_bow = get_bow_representation_matrix(train_data[:2500], list_of_selected_words, 'l')
#
# # luam reprezentarea bow pt datele de testare
# data_test_bow = get_bow_representation_matrix(data_test, list_of_selected_words, 'l')
#
# # Clasificator
#
# # definire
# clasificator1 = GaussianNB()
#
# # antrenare
# date1 = time.time()
# clasificator1.fit(data_train_validation_bow, labels_train[:2500])
# date2 = time.time()
#
# # timp antrenare
# timp = date2 - date1
# print("Timp antrenare", timp)
#
# # predictii
# predicted_labels_test = clasificator1.predict(data_test_bow)
#
# # acuratete
# print("Acuratete", acuratete(predicted_labels_test, test_labels))
